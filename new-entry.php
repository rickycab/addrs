<?php include('includes/head.php'); ?>

<?php include('includes/nav.php'); ?>


<div class="container">
	<div class="row main cf">
		<h1 class="page-title"><?php echo $newEntryTitle; ?></h1>
			<?php include('includes/entry-form.php'); ?>
		</div>
	</div>
</div>