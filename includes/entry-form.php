<form action="includes/input.php" method="post">
			<div class="half">
				<h3 class="form-section-title">Basic Information</h3>
				<div class="single-form">
					<label>Pick Address Book</label><br />
					<select id="entryPickBook" name="entryPickBook"/>
						<option value="null">Pick a book</option>
						<option value="friends">Friends</option>
						<option value="family">Family</option>
						<option value="work">Work</option>
					</select>
				</div>
				<div class="single-form">
					<label>First Name</label><br />
					<input class="entry-input" type="text" name="entryFirstName" id="entryFirstName" />
				</div>
				<div class="single-form">
					<label>Middle Name</label><br />
					<input class="entry-input" type="text" name="entryMiddleName" id="entryMiddleName" />
				</div>
				<div class="single-form">
					<label>Last Name</label><br />
					<input class="entry-input" type="text" name="entryLastName" id="entryLastName" />
				</div>
				<div class="single-form">
					<label>Home Address</label><br />
					<input class="entry-input" type="text" name="entryHomeAddress" />
				</div>
				<div class="single-form">
					<label>Work Address</label><br />
					<input class="entry-input" type="text" name="entryWorkAddress" />
				</div>
				<div class="single-form">
					<label>Home Phone</label><br />
					<input class="entry-input" type="text" name="entryHomePhone" />
				</div>
				<div class="single-form">
					<label>Cell Phone</label><br />
					<input class="entry-input" type="text" name="entryCellPhone" />
				</div>
				<div class="single-form">
					<label>Work Phone</label><br />
					<input class="entry-input" type="text" name="entryWorkPhone" />
				</div>
				<div class="single-form">
					<label>Email</label><br />
					<input class="entry-input" type="text" name="entryEmail" />
				</div>

			</div>
			<div class="half last">
				<h3 class="form-section-title">Social Networks</h3>
				<div class="single-form">
					<label>Facebook</label><br />
					<input class="entry-input" type="text" name="entryFacebook" />
				</div>
				<div class="single-form">
					<label>Twitter</label><br />
					<input class="entry-input" type="text" name="entryTwitter" />
				</div>
				<div class="single-form">
					<label>Instagram</label><br />
					<input class="entry-input" type="text" name="entryInstagram" />
				</div>
				<div class="single-form">
					<label>LinkedIn</label><br />
					<input class="entry-input" type="text" name="entryLinkedIn" />
				</div>
				<div class="single-form">
					<label>Github</label><br />
					<input class="entry-input" type="text" name="entryGithub" />
				</div>
				<input type="submit" value="submit"/>
			</form>