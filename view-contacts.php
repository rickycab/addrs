<?php 
include('includes/head.php');  
include('includes/nav.php'); 
$client = $_GET['clientID'];
?>
<div class="container">
	<div class="row">
		<div class="main">
			<div class="full">
				<h1 class="page-title">All Contacts</h1>
				<ul class="contact-list">
					<?php include('includes/retrieve-summary-data.php'); ?>
				</ul>
			</div>
		</div>
	</div>
</div>